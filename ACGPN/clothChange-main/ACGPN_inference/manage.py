##########################基于Flask设计页面
from flask import request,jsonify,Flask,render_template

import webbrowser
import cv2
import numpy as np
# from flask_script import Manager
import time
import matplotlib.pyplot as plt
import random
import string
from PIL import Image,ImageFont,ImageDraw
from io import BytesIO
from datetime import datetime
import uuid
import os
from utils1 import clip_img,ident_edge

app = Flask(__name__)
app.config['SECRET_KEY'] = 'mysoft'
# webbrowser.open('http://127.0.0.1:5000/', new=1, autoraise=True) 
@app.route('/',methods=["POST"])
def index():
    # 获取前端照片，使用Ajax方式提交
    user_image = request.files.get('user')
    clothes_image = request.files.get('clothes')
    imglist = [user_image,clothes_image]
    imgdir = ['./imgs_web/userimg','./imgs_web/clothesimg']
    for i in range(2):
        if  imglist[i] != None :
            img =  imglist[i].read()
            #图片解码显示,并保存
            img_np_arr = np.frombuffer(img, np.uint8)
            image = cv2.imdecode(img_np_arr, cv2.IMREAD_COLOR)
            if os.path.isdir(imgdir[i]) == False:
                 os.makedirs(imgdir[i])
            #将上传的照片保存
            file = imgdir[i]+'/'+imglist[i].filename
            cv2.imwrite(file,image)
            
            #对图片进行处理
            if i == 0:
                image = clip_img(file)
                cv2.imwrite('../ACGPN_inference/sourceImage.jpg', image)
            if i == 1:
                # print(1)
                image = clip_img(file)
                cv2.imwrite('../Data_preprocessing/clothes/1.jpg', image)
                # print(1)
                ident_edge(image,'../Data_preprocessing/edges/1.jpg')
    os.system('python ../ACGPN_inference/clothChange.py')
    image_result = cv2.imread('../ACGPN_inference/sample/sourceImage.jpg')
    # return render_template('index.html')
    #合成后的照片地址
    data = {"avatar_url": 'sample/sourceImage.jpg'}
    return jsonify(errno=0, errmsg='错误', data=data)

@app.route('/',methods=["get"])
def index1():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')

