/*
* @Author: 86131
* @Date:   2022-04-11 14:48:38
* @Last Modified by:   86131
* @Last Modified time: 2022-04-19 14:51:10
*/


// 点击背景div消失出现
var inshow = true

$(document).ready(function(e) {
    $("body").click(function(){
          if (inshow){
		       $(".box").hide();
		       inshow = false;}
		  else{
		       $(".box").show();
		       inshow = true;}
        });
  //   $('body').load(function() {
  //   	//自动换图
  //   	var z=1;
		// //设置为每三秒切换一次
  //   	var iInterval=setInterval( "autoChange(obj)" ,3000);
	 //    function autoChange(obj){
	 //        z++;
		// 	//因为只有6张背景图片，所以每当计数到7，即返回初始状态
	 //        if (z==3){z=1;}
	 //        var oImg= "background" +z+ ".png)";
	 //        $('body').css('background':oImg)
		// } 
  //   });
    $(".box").click(function(e){
           e.stopPropagation();//阻止事件冒泡
        });
    
});


// 显示预览图片
$(".user input:file").change(function(){
        var reader = new FileReader();		//获取一个FileReader类
        var file = this.files[0];			//获取用户选择的文件
        reader.readAsDataURL(file);			//开始读取指定Blob（二进制文件）中的内容。一旦完成，立即触发 loadend 事件，并且该对象的 result 属性中将包含一个 data: URL 格式的字符串以表示所读取文件的内容。
        reader.onloadend = function(){
            $(".user img").attr("src", reader.result);
        }
});

$(".clothes input:file").change(function(){
        var reader = new FileReader();		//获取一个FileReader类
        var file = this.files[0];			//获取用户选择的文件
        reader.readAsDataURL(file);			//开始读取指定Blob（二进制文件）中的内容。一旦完成，立即触发 loadend 事件，并且该对象的 result 属性中将包含一个 data: URL 格式的字符串以表示所读取文件的内容。
        reader.onloadend = function(){
            $(".clothes img").attr("src", reader.result);
        }
});

// 向后端返回图片
$(document).ready(function(){
	$("#form-avatar").submit(function(e){
		e.preventDefault();
		$(this).ajaxSubmit({
			url :'/',
			type:'post',
			dataType:'json',
			// headers:{
			// "X-CSRFToken":getCookie()
			// },
			success:function(data){
				if (data.errno == '0'){
					var avatar_url = data.data.avatar_url
					$('.cross_dressing').css('display','block')
					$('.cross').css('display','block')
					$("#cross_dressing").attr('src',avatar_url)}
				else{
					alert(data.errmsg)}
			}
		})
	})
});

// $(document).ready(function(){
// 	$("#form-avatar").submit(function(e){
// 		e.preventDefault();
// 		$(this).ajaxSubmit({
// 			url :'/',
// 			type:'get',
// 			dataType:'json',
// 			success:function(data){
// 				if (data.errno == '0'){
// 					var avatar_url = data.data.avatar_url
// 					$("#cross_dressing").attr('src',avatar_url)}
// 				else{
// 					alert(data.errmsg)}
// 			}
// 		})
// 	})
// });
