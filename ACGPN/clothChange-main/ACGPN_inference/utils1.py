# import  mediapipe
import cv2
from removebg import RemoveBg
import os
from PIL import Image
import matplotlib.pyplot as plt
import numpy as np

#图片裁剪为192*256
# def clip_img(file):
#     rmbg = RemoveBg("UHBhnuFougDSMd1Pd3imwDKS", "error.log")  # 引号内是你获取的API
#     rmbg.remove_background_from_img_file(file, bg_color='white')  # 图片地址
#     new_file = file+"_no_bg.png"
#     image = cv2.imread(new_file)
#     h,w,_ = image.shape#三通道
#     # print(h,w)
#     min_border = min(w,h)
#     # print(min_border)
#     #固定最小边放缩
#     if w<h:
#         resize_img = cv2.resize(image, (w*192//min_border,h*192//min_border), interpolation=cv2.INTER_LINEAR)
#     else:
#         resize_img = cv2.resize(image, (w * 256// min_border, h * 256// min_border), interpolation=cv2.INTER_LINEAR)
#
#     new_h,new_w,_ = resize_img.shape
#     # print(new_h,new_w)
#     image = resize_img
#     lab = 250
#     if new_h== 256:
#         ret = new_w - 192
#         for i in range(ret):
#             #两边均切
#             if i%2 == 0:
#                 # print(resize_img[:, i].mean())
#                 if resize_img[:,i].mean() <lab:
#
#                     image = image[:,1:]
#                 elif resize_img[:,-1-i].mean() <lab:
#
#                     image = image[:,:-1]
#             else:
#                 if resize_img[:, -1-i].mean() <lab:
#
#                     image = image[:, :-1]
#                 elif lab>resize_img[:,i].mean() :
#
#                     image = image[:,1:]
#
#     if new_w== 192:
#         ret = new_h - 256
#         for i in range(ret):
#             # print(resize_img[i,:].mean())
#             if i%2 == 0:
#                 if lab>resize_img[i,:].mean() :
#                     image = image[1:,:]
#                 elif lab>resize_img[-1-i,:].mean() :
#                     image = image[:-1,:]
#             else:
#                 if lab>resize_img[-1-i,:].mean():
#                     image = image[:-1,:]
#                 elif lab>resize_img[i,:].mean():
#                     image = image[1:,:]
#     # cv2.imwrite(file+'_resize.png', image)
#     return image

def clip_img(file):
    # rmbg = RemoveBg("UHBhnuFougDSMd1Pd3imwDKS", "error.log")  # 引号内是你获取的API
    # rmbg.remove_background_from_img_file(file, bg_color='white')  # 图片地址
    # new_file = file+"_no_bg.png"
    image = cv2.imread(file)
    h,w,_ = image.shape#三通道
    print(h,w)
    min_border = min(w,h)
    print(min_border)
    #固定最小边放缩
    if w<h:
        resize_img = cv2.resize(image, (w*192//min_border,h*192//min_border), interpolation=cv2.INTER_LINEAR)
    else:
        resize_img = cv2.resize(image, (w * 256// min_border, h * 256// min_border), interpolation=cv2.INTER_LINEAR)

    new_h,new_w,_ = resize_img.shape
    # print(new_h,new_w)
    image = resize_img
    lab = 240
    if new_h== 256:

        ret = new_w - 192
        for i in range(ret):
            #两边均切
            if i%2 == 0:
                # print(resize_img[:, i].mean())
                if resize_img[:,i].mean()>lab:

                    image = image[:,1:]
                elif resize_img[:,-1-i].mean() >lab:
                    image = image[:,:-1]
            else:
                if resize_img[:, -1-i].mean() >lab:

                    image = image[:, :-1]
                elif resize_img[:,i].mean() >lab:

                    image = image[:,1:]

    if new_w== 192:
        ret = new_h - 256
        for i in range(ret):
            # print(resize_img[i,:].mean())
            if i%2 == 0:
                if resize_img[i,:].mean() >lab:
                    image = image[1:,:]
                elif resize_img[-1-i,:].mean() >lab:
                    image = image[:-1,:]
            else:
                if resize_img[-1-i,:].mean()>lab:
                    image = image[:-1,:]
                elif resize_img[i,:].mean()>lab:
                    image = image[1:,:]
    # cv2.imwrite(file+'_resize.png', image)
    return image

def ident_edge(img,filename):
    #rgb-gray
    mask = img.copy()
    img = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
    #sobel算子
    x = cv2.Sobel(img, cv2.CV_16S, 1, 0)  # 对x求一阶导
    y = cv2.Sobel(img, cv2.CV_16S, 0, 1)  # 对y求一阶导
    absX = cv2.convertScaleAbs(x)
    absY = cv2.convertScaleAbs(y)
    Sobel = cv2.addWeighted(absX, 0.5, absY, 0.5, 0)
    #gray-binary
    ret, binary = cv2.threshold(Sobel,10, 255, cv2.THRESH_BINARY)
    #边界提取
    contours, hierarchy = cv2.findContours(binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    area = []
    # 找到最大的轮廓
    for k in range(len(contours)):
        area.append(cv2.contourArea(contours[k]))
    max_idx = np.argmax(np.array(area))
    # print(max_idx)
    # 填充最大的轮廓
    mask = cv2.drawContours(binary, contours, max_idx, 255, cv2.FILLED)
    mask = cv2.medianBlur(mask, 7)
    cv2.imwrite(filename,mask)
    return mask

if __name__ == '__main__':
    img = clip_img('9.jpg')
    cv2.imwrite('./ioo.jpg',img)
    cv2.imshow('ed',img)
    cv2.waitKey()

