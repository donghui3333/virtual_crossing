# 虚拟换装

#### 介绍
基于ACGAN实现虚拟换装，以web网页做交互式处理

#### 软件架构
软件架构说明

前端使用flask做交互处理

使用U2net对图片数据进行提取前景，提取轮廓，裁剪等处理

使用alphapose对图像进行骨骼点检测，并将检测后的图像保存为json文件

Self Correction for Human Parsing 解析人体，获得人体解析后的图像

使用ACGPN对处理过后的图片数据进行合并


#### 使用说明

manage.py为启动文件

U2net的权重文件包括普通型u2net和轻量型u2netp，本次只基于轻量型进行使用。

alphapose用到预训练模型fast_421_res152_256x192.pth，Configure 文件为 256x192_res152_lr1e-3_1x-duc.yaml
用到的目标检测算法为EfficientNet，使用的预训练模型为tf-efficientnet-d4(代码文件中命名为efficientnet-d4)

Self Correction for Human Parsing 使用到预训练模型为exp-schp-201908261155-lip.pth
