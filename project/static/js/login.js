/*
* @Author: 86131
* @Date:   2022-04-11 14:48:38
* @Last Modified by:   86131
* @Last Modified time: 2022-05-08 14:06:49
*/


// 点击背景div消失出现
var inshow = false

$(document).ready(function(e) {
    $("body").click(function(){
          if (inshow){
		       $(".box").hide();
		       inshow = false;}
		  else{
		       $(".box").show();
		       inshow = true;}
        });
  

    $(".box").click(function(e){
           e.stopPropagation();//阻止事件冒泡
        });
   
});
// 清空图片
$("button").click(function(){
		$("#foo").html('');
        $("#foo1").html('');
        $('.cross_dressing').css('display','none');
        $('.cross').css('display','none');
        $("#cross_dressing").attr('src','')
});
// 自动轮换背景图
// window.onload = function() {
//       var i = 1;
//       document.body.style.backgroundImage  = 'url(./../static/background' +i+'.png)';
//       function time(){
//         i++;
//         i = i%3
//         document.body.style.backgroundImage  = 'url(./../static/background' +i+'.png)';
//       }
//       setInterval(time, 5000);
//     };
// 显示预览图片
// $(".user input:file").change(function(){
//         var reader = new FileReader();		//获取一个FileReader类
//         var file = this.files[0];			//获取用户选择的文件
//         reader.readAsDataURL(file);			//开始读取指定Blob（二进制文件）中的内容。一旦完成，立即触发 loadend 事件，并且该对象的 result 属性中将包含一个 data: URL 格式的字符串以表示所读取文件的内容。
//         reader.onloadend = function(){
//             $(".user img").attr("src", reader.result);
//         }
// });

// $(".clothes input:file").change(function(){
//         var reader = new FileReader();		//获取一个FileReader类
//         var file = this.files[0];			//获取用户选择的文件
//         reader.readAsDataURL(file);			//开始读取指定Blob（二进制文件）中的内容。一旦完成，立即触发 loadend 事件，并且该对象的 result 属性中将包含一个 data: URL 格式的字符串以表示所读取文件的内容。
//         reader.onloadend = function(){
//             $(".clothes img").attr("src", reader.result);
//         }
// });

// 向后端返回图片
$(document).ready(function(){
	$("#form-avatar").submit(function(e){
		e.preventDefault();

        if ($("#foo").html().length == 0  || $("#foo1").html().length == 0){
            alert('请检查人物与衣物图片是否全部上传');
            return;
        }
        // 启动等待
        $('.cross_dressing').css('display','block');
        $('.cross').css('display','block');
        $("#cross_dressing").attr('src','static/1441607086860827.gif');
		$(this).ajaxSubmit({
			url :'/',
			type:'post',
			dataType:'json',
			// headers:{
			// "X-CSRFToken":getCookie()
			// },

			success:function(data){
				if (data.errno == '66'){
					var avatar_url = data.data.avatar_url
					$('.cross_dressing').css('display','block')
					$('.cross').css('display','block')
					$("#cross_dressing").attr('src',avatar_url)}
				else{
					alert(data.errmsg)
                }
			}
		})
	})
});

// 拖拽上传并显示
var userAgent = navigator.userAgent; //用于判断浏览器类型

$(".file").change(function() {

    //获取选择图片的对象

    var docObj = $(this)[0];

    var foo = $("#foo");

    //得到所有的图片文件

    var fileList = docObj.files;

	//添加时间戳

	var dateTime = new Date().getTime();

	//fileList.lastModified =dateTime;

	    //循环遍历

    for (var i = 0; i < fileList.length; i++) {

        //动态添加html元素

        var picHtml = "<div class='imageDiv' > <img id='img_"+ dateTime + "_" + fileList[i].name + "' /> <div class='cover'><i class='delbtn'>删除</i></div></div>";

        console.log(picHtml);

        //foo.prepend(picHtml);//prepend() - 在被选元素的开头插入内容

		foo.append(picHtml);//append() - 在被选元素的结尾插入内容

        //获取图片imgi的对象

        var imgObjPreview = document.getElementById("img_"+ dateTime + "_" + fileList[i].name);

        if (fileList && fileList[i]) {

            //图片属性

            imgObjPreview.style.display = 'block';

            imgObjPreview.style.width = '160px';

            imgObjPreview.style.height = '130px';

            //imgObjPreview.src = docObj.files[0].getAsDataURL();

            //火狐7以上版本不能用上面的getAsDataURL()方式获取，需要以下方式

            if (userAgent.indexOf('MSIE') == -1) {

                //IE以外浏览器

                imgObjPreview.src = window.URL.createObjectURL(docObj.files[i]); //获取上传图片文件的物理路径;

                console.log(imgObjPreview.src);

                // var msgHtml = '<input type="file" id="fileInput" multiple/>';

            } else {

                //IE浏览器

                if (docObj.value.indexOf(",") != -1) {

                    var srcArr = docObj.value.split(",");

                    imgObjPreview.src = srcArr[i];

                } else {

                    imgObjPreview.src = docObj.value;

                }
            }
        }
    }
    /*删除功能*/
    $(".delbtn").click(function() {

        var imageDiv = $(this);

        imageDiv.parents(".imageDiv").remove();

    });

});

 

$(".file1").change(function() {

    //获取选择图片的对象

    var docObj = $(this)[0];

    var foo = $("#foo1");

    //得到所有的图片文件

    var fileList = docObj.files;

	//添加时间戳

	var dateTime = new Date().getTime();

	//fileList.lastModified =dateTime;

	    //循环遍历

    for (var i = 0; i < fileList.length; i++) {

        //动态添加html元素

        var picHtml = "<div class='imageDiv' > <img id='img_"+ dateTime + "_" + fileList[i].name + "' /> <div class='cover'><i class='delbtn'>删除</i></div></div>";

        console.log(picHtml);

        //foo.prepend(picHtml);//prepend() - 在被选元素的开头插入内容

		foo.append(picHtml);//append() - 在被选元素的结尾插入内容

        //获取图片imgi的对象

        var imgObjPreview = document.getElementById("img_"+ dateTime + "_" + fileList[i].name);

        if (fileList && fileList[i]) {

            //图片属性

            imgObjPreview.style.display = 'block';

            imgObjPreview.style.width = '160px';

            imgObjPreview.style.height = '130px';

            //imgObjPreview.src = docObj.files[0].getAsDataURL();

            //火狐7以上版本不能用上面的getAsDataURL()方式获取，需要以下方式

            if (userAgent.indexOf('MSIE') == -1) {

                //IE以外浏览器

                imgObjPreview.src = window.URL.createObjectURL(docObj.files[i]); //获取上传图片文件的物理路径;

                console.log(imgObjPreview.src);

                // var msgHtml = '<input type="file" id="fileInput" multiple/>';

            } else {

                //IE浏览器

                if (docObj.value.indexOf(",") != -1) {

                    var srcArr = docObj.value.split(",");

                    imgObjPreview.src = srcArr[i];

                } else {

                    imgObjPreview.src = docObj.value;

                }
            }
        }
    }
    /*删除功能*/
    $(".delbtn").click(function() {

        var imageDiv = $(this);

        imageDiv.parents(".imageDiv").remove();

    });

});

 

