##########################基于Flask设计页面
from flask import request,jsonify,Flask,render_template
import cv2
import numpy as np
from flask_script import Manager
import time
import matplotlib.pyplot as plt
import random
import string
from PIL import Image,ImageFont,ImageDraw
from io import BytesIO
from datetime import datetime
import uuid
import os
import shutil
from utils import clip_img
from u2net_test import removebg
app = Flask(__name__)
app.config['SECRET_KEY'] = 'mysoft'

@app.route('/',methods=["POST"])
def index():
    # 获取前端照片，使用Ajax方式提交
    user_image = request.files.getlist('user')
    clothes_image = request.files.getlist('clothes')

    imglist = [user_image,clothes_image]
    imgdir = ['./images/userimg','./images/clothesimg']


    for i in range(2):
        #空则创，有则删
        if os.path.isdir(imgdir[i]) == False:
                 os.makedirs(imgdir[i])
        else:
            for j in os.listdir(imgdir[i]):
                path_file = os.path.join(imgdir[i], j)
                if os.path.isfile(path_file):
                    os.remove(path_file)
                else:
                    shutil.rmtree(path_file, ignore_errors=True)

        if  imglist[i] != [] :
            for j in imglist[i]:
                img =  j.read()
                #图片解码显示,并保存
                img_np_arr = np.frombuffer(img, np.uint8)
                image = cv2.imdecode(img_np_arr, cv2.IMREAD_COLOR)

                #将上传的照片保存,排除中文，
                uid = str(uuid.uuid4())
                suid = ''.join(uid.split('-'))
                file = imgdir[i]+'/'+suid+'.'+j.filename.split('.')[-1]
                cv2.imwrite(file,image)

            #对图片进行处理
            removebg(imgdir[i])


    #合成后的照片地址
    data = {"avatar_url": 'static/background1.png'}
    return jsonify(errno=66, errmsg='错误', data=data)

@app.route('/',methods=["get"])
def index1():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True,port=5000)

