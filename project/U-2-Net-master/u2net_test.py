import os
from skimage import io, transform
import torch
import torchvision
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms  # , utils
# import torch.optim as optim
import cv2
import numpy as np
from PIL import Image
import glob

from data_loader import RescaleT
from data_loader import ToTensor
from data_loader import ToTensorLab
from data_loader import SalObjDataset  # 加载数据用（返回图片的索引，归一化后的图片，标签）

from model import U2NET  # full size version 173.6 MB         #导入两个网络
from model import U2NETP  # small version u2net 4.7 MB
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True#图片压缩范围超过限制了，PIL处理不了，必须要把这个图片删除一部分
from utils import clip_img

#检查图片类型并更名
def is_type_wrong(path):
    '''
    检查文件后缀是否与实际对应，例如实际是jpg，后缀是gif，导致打不开
    '''
    real_type = path[path.rfind('.') + 1:]
    # print(real_type)
    if path.lower().endswith('.gif') or path.lower().endswith('.jpg') or path.lower().endswith('.png'):
        header = []
        with open(path, 'rb') as f:
            while (len(header) < 5):
                header.append(f.read(1))
        tmp = 'jpg'
        if (header[0] == '\x47' and header[1] and '\x49' and header[2] == '\x46' and header[3] == '\x38'):
            tmp = 'gif'
        if (header[0] == '\xff' and header[1] == '\xd8'):
            tmp = 'jpg'
        if (header[0] == '\x89' and header[1] == '\x50' and header[2] == '\x4e' and header[3] == '\x47' and header[
            4] == '\x0D'):
            tmp = 'png'
        # print(tmp)
        if real_type != tmp:
            return True, tmp
    return False, real_type


# normalize the predicted SOD probability map
def normPRED(d):  # 归一化
    ma = torch.max(d)
    mi = torch.min(d)

    dn = (d - mi) / (ma - mi)

    return dn

#保存无背景图片，与轮廓图片
def save_output(image_name, pred, d_dir,d_dir2):
    predict = pred.squeeze()  # 删除单维度
    predict_np = predict.cpu().data.numpy()  # 转移到CPU上

    im = Image.fromarray(predict_np * 255).convert('RGB')  # 转为PIL，从归一化的图片恢复到正常0到255之间

    img_name = image_name.split("\\")[-1]  # 取出图片类型

    image = io.imread(image_name)  # io.imread读出图片格式是uint8(unsigned int)；value是numpy array；图像数据是以RGB的格式进行存储的，通道值默认范围0-255

    imo = im.resize((image.shape[1], image.shape[0]), resample=Image.BILINEAR)

    aaa = img_name.split(".")  # 图片名字被切分为一个列表
    bbb = aaa[0:-1]  # 取出图片名称的前缀
    # print("---------------------------------------------")
    imidx = bbb[0]
    for i in range(1, len(bbb)):
        imidx = imidx + "." + bbb[i]
    #保存轮廓图片
    imo.save(d_dir + imidx + '.png')  # 保存图片到指定路径
    mask = cv2.imread(d_dir + imidx + '.png',0)  # 以灰度读入
    imo = clip_img(imo)
    cv2.imwrite(d_dir + imidx + '.png', imo)  # 保存图片到指定路径
    print('save_')
    # 保存无背景照片
    height, width, channel = image.shape
    b, g, r = cv2.split(image)
    dstt = np.zeros((4, height, width), dtype=image.dtype)

    dstt[0][0:height, 0:width] = r
    dstt[1][0:height, 0:width] = g
    dstt[2][0:height, 0:width] = b
    dstt[3][0:height, 0:width] = mask

    im1 = clip_img(cv2.merge(dstt))
    cv2.imwrite(d_dir2 +imidx + '.png',im1)


def removebg(datadir):
    # --------- 1. get image path and name ---------
    model_name = 'u2netp'  # u2netp                              #保存的模型的名称
    #轮廓路径
    prediction_dir_edge = os.path.join(datadir + '_edge' + os.sep)
    #背景路径
    prediction_dir_nobg = os.path.join(datadir + '_nobg' + os.sep)
    # 模型路径
    model_dir = os.path.join(os.getcwd(), 'U-2-Net-master\saved_models', model_name, model_name + '.pth')

    if os.path.isdir(prediction_dir_edge) == False:
        os.makedirs(prediction_dir_edge)

    if os.path.isdir(prediction_dir_nobg) == False:
        os.makedirs(prediction_dir_nobg)

    img_name_list = glob.glob(datadir + os.sep + '*')

    for i in range(len(img_name_list)):
        aaa = img_name_list[i].split(".")  # 图片名字被切分为一个列表
        bbb = aaa[0:-1]  # 取出图片名称的前缀
        is_wrong, real_type = is_type_wrong(img_name_list[i])
        if is_wrong:
            os.rename(img_name_list[i],'.'.join(bbb)+'.'+real_type)
            img_name_list[i] = '.'.join(bbb) + '.' + real_type

    print(img_name_list)

    # --------- 2. dataloader ---------
    # 1. dataloader
    test_salobj_dataset = SalObjDataset(img_name_list=img_name_list,
                                        lbl_name_list=[],
                                        transform=transforms.Compose([RescaleT(320),
                                                                      ToTensorLab(flag=0)])
                                        )
    test_salobj_dataloader = DataLoader(test_salobj_dataset,
                                        batch_size=1,
                                        shuffle=False,
                                        num_workers=1)  # 加载数据

    # --------- 3. model define ---------
    if (model_name == 'u2net'):  # 分辨使用的是哪一个模型参数
        print("...load U2NET---173.6 MB")
        net = U2NET(3, 1)
    elif (model_name == 'u2netp'):
        print("...load U2NEP---4.7 MB")
        net = U2NETP(3, 1)
    net.load_state_dict(torch.load(model_dir))  # 加载训练好的模型
    if torch.cuda.is_available():
        net.cuda()  # 网络转移至GPU
    net.eval()  # 测评模式

    # --------- 4. inference for each image ---------
    for i_test, data_test in enumerate(test_salobj_dataloader):

        print("inferencing:", img_name_list[i_test].split("/")[-1])  # test_images\5.jpg
                                 #'imidx': tensor([[0]], dtype=torch.int32), 'image': tensor([[[[ 1.4051,  ...'label': tensor([[[[0., 0., 0.,  ...,
        inputs_test = data_test['image']  # 测试的是图片
        inputs_test = inputs_test.type(torch.FloatTensor)  # 转为浮点型

        if torch.cuda.is_available():
            inputs_test = Variable(inputs_test.cuda())
            # Variable是对Tensor的一个封装，操作和Tensor是一样的，但是每个Variable都有三个属性，
            # tensor不能反向传播，variable可以反向传播。它会逐渐地生成计算图。
            # 这个图就是将所有的计算节点都连接起来，最后进行误差反向传递的时候，
            # 一次性将所有Variable里面的梯度都计算出来，而tensor就没有这个能力
        else:
            inputs_test = Variable(inputs_test)

        d1, d2, d3, d4, d5, d6, d7 = net(inputs_test)  # 将图片传入网络

        # normalization
        pred = d1[:, 0, :, :]
        pred = normPRED(pred)  # 对预测的结果做归一化

        # save results to test_results folder
        save_output(img_name_list[i_test], pred, prediction_dir_edge,prediction_dir_nobg)  # 原始图片名、预测结果，预测图片的保存目录   #save_output保存预测的输出值

        del d1, d2, d3, d4, d5, d6, d7  # del 用于删除对象。在 Python，一切都是对象，因此 del 关键字可用于删除变量、列表或列表片段等。


if __name__ == "__main__":
    # main()
    removebg()