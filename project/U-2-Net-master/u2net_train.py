import torch
import torchvision
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F

from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import torch.optim as optim
import torchvision.transforms as standard_transforms

import numpy as np
import glob
import os

from data_loader import RescaleT, RandomCrop, ToTensorLab, SalObjDataset, ToTensor, Rescale
from model import U2NET, U2NETP

# ------- 1. define loss function --------

bce_loss = nn.BCELoss(reduction='mean')


def muti_bce_loss_fusion(d0, d1, d2, d3, d4, d5, d6, labels_v):
    loss0 = bce_loss(d0, labels_v)
    loss1 = bce_loss(d1, labels_v)
    loss2 = bce_loss(d2, labels_v)
    loss3 = bce_loss(d3, labels_v)
    loss4 = bce_loss(d4, labels_v)
    loss5 = bce_loss(d5, labels_v)
    loss6 = bce_loss(d6, labels_v)

    loss = loss0 + loss1 + loss2 + loss3 + loss4 + loss5 + loss6  # 在网路结构中共有6个sup所以有6个损失函数
    print("l0: %3f, l1: %3f, l2: %3f, l3: %3f, l4: %3f, l5: %3f, l6: %3f\n" % (
    loss0.item(), loss1.item(), loss2.item(), loss3.item(), loss4.item(), loss5.item(), loss6.item()))

    return loss0, loss


def main():
    # ------- 2. set the directory of training dataset --------
    model_name = 'u2net'  # 'u2netp'                                     #选取的网络模型的种类

    # params_path = os.path.join("../saved_models", model_name,model_name.pth)

    data_dir = r'F:\PASCAL_VOC\VOCdevkit\VOC2007\my_segmentations'  # 包含训练图片和分割标签的上一级目录
    tra_image_dir = 'JPEGImages'  # 图片所在的目录名
    tra_label_dir = 'SegmentationClass'  # 标签所在的目录名

    image_ext = '.jpg'
    label_ext = '.png'  # 标签的后缀名

    model_dir = './saved_models/' + model_name + '/'  # 保存的参数模型所在的文件夹名
    params_path = model_dir + model_name + ".pth"  # 保存的参数参数的相对路径
    epoch_num = 100000  # 训练的总轮次
    batch_size_train = 2  # 训练的批次
    batch_size_val = 1  # 验证的批次

    train_num = 0
    val_num = 0

    # tra_img_name_list = glob.glob(data_dir + "\\" + tra_image_dir + '*')
    tra_img_name_list = glob.glob(os.path.join(data_dir, tra_image_dir, '*'))  # 训练的图片所在的路径，glob.glob()将图片的绝对路径保存到一个列表
    print("hahah")
    # print(tra_img_name_list)                                                   #包含所有训练图片绝对路径的列表
    # print("-------------------------------------------------------------------------------------------------")
    tra_lbl_name_list = []
    for img_path in tra_img_name_list:  # 遍历每一个图片的绝对路径
        img_name = img_path.split("\\")[-1]  # 取出图片的名字，如：003000.jpg
        aaa = img_name.split(".")
        bbb = aaa[0:-1]  # ['000032']
        # print(bbb)
        # 去除后缀的图片名
        imidx = bbb[0]  # 000032
        # print(imidx)                                                         #000032
        # print(len(bbb))                                                      #1
        for i in range(1, len(bbb)):
            print(i)
            imidx = imidx + "." + bbb[i]
            print(imidx, "**********")

        tra_lbl_name_list.append(data_dir + "\\" + tra_label_dir + "\\" + imidx + label_ext)
    print(tra_lbl_name_list)  # 标签的绝对路径的列表，和训练图片的绝对路径一一对应

    print("---")
    print("train images: ", len(tra_img_name_list))  # 422
    print("train labels: ", len(tra_lbl_name_list))
    print("---")

    train_num = len(tra_img_name_list)  # 训练的图片的总数

    salobj_dataset = SalObjDataset(
        img_name_list=tra_img_name_list,
        lbl_name_list=tra_lbl_name_list,
        transform=transforms.Compose([
            RescaleT(320),
            RandomCrop(288),
            ToTensorLab(flag=0)]))  # RescaleT(320)等比缩放为指定的大小，RandomCrop(288)随机裁剪为指定的大小
    salobj_dataloader = DataLoader(salobj_dataset, batch_size=batch_size_train, shuffle=True, num_workers=1)

    # ------- 3. define model --------
    # define the net
    if (model_name == 'u2net'):
        net = U2NET(3, 1)  # 指定输入通道核输出通道的大小
    elif (model_name == 'u2netp'):  # 网络实例化
        net = U2NETP(3, 1)

    if torch.cuda.is_available():
        net.cuda()  # 网络转移至GPU

    if os.path.exists(params_path):  # 加载训练好的模型参数
        net.load_state_dict(torch.load(params_path))
    else:
        print("No parameters!")

    # ------- 4. define optimizer --------
    print("---define optimizer...")
    optimizer = optim.Adam(net.parameters(), lr=0.001, betas=(0.9, 0.999), eps=1e-08, weight_decay=0)

    # ------- 5. training process --------
    print("---start training...")
    ite_num = 0
    running_loss = 0.0
    running_tar_loss = 0.0
    ite_num4val = 0
    save_frq = 2000  # save the model every 2000 iterations

    for epoch in range(0, epoch_num):
        net.train()  # 训练模式

        for i, data in enumerate(salobj_dataloader):
            ite_num = ite_num + 1
            ite_num4val = ite_num4val + 1

            inputs, labels = data['image'], data['label']  # 获取图片和标签

            inputs = inputs.type(torch.FloatTensor)  # 转为tensor类型
            labels = labels.type(torch.FloatTensor)

            # wrap them in Variable
            if torch.cuda.is_available():  # 转移到cuda
                inputs_v, labels_v = Variable(inputs.cuda(), requires_grad=False), Variable(labels.cuda(),
                                                                                            requires_grad=False)
            else:
                inputs_v, labels_v = Variable(inputs, requires_grad=False), Variable(labels, requires_grad=False)

            # y zero the parameter gradients
            optimizer.zero_grad()  # 优化器清空梯度

            # forward + backward + optimize
            d0, d1, d2, d3, d4, d5, d6 = net(inputs_v)  # 网络输出
            loss2, loss = muti_bce_loss_fusion(d0, d1, d2, d3, d4, d5, d6, labels_v)  # 6个sup作损失

            loss.backward()  # 反向求导更新梯度
            optimizer.step()  # 下一步

            # # print statistics
            running_loss += loss.item()  # 总损失
            running_tar_loss += loss2.item()

            # delete temporary outputs and loss
            del d0, d1, d2, d3, d4, d5, d6, loss2, loss

            print("[epoch: %3d/%3d, batch: %5d/%5d, ite: %d] train loss: %3f, tar: %3f " % (
                epoch + 1, epoch_num, (i + 1) * batch_size_train, train_num, ite_num, running_loss / ite_num4val,
                running_tar_loss / ite_num4val))

            if ite_num % save_frq == 0:
                # torch.save(net.state_dict(), model_dir + model_name+"_bce_itr_%d_train_%3f_tar_%3f.pth" % (ite_num, running_loss / ite_num4val, running_tar_loss / ite_num4val))
                running_loss = 0.0
                running_tar_loss = 0.0
                net.train()  # resume train
                ite_num4val = 0
        # torch.save(net.state_dict(), model_dir + model_name+"_bce_itr_%d_train_%3f_tar_%3f.pth" % (ite_num, running_loss / ite_num4val, running_tar_loss / ite_num4val))
        torch.save(net.state_dict(), params_path)


if __name__ == "__main__":
    main()